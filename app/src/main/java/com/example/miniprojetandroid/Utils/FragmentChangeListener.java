package com.example.miniprojetandroid.Utils;

import androidx.fragment.app.Fragment;

public interface FragmentChangeListener {
    public void replaceFragment(Fragment fragment);
}
