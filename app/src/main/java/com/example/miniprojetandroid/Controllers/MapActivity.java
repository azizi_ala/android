package com.example.miniprojetandroid.Controllers;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;

import com.example.miniprojetandroid.R;
//import com.mapbox.mapboxsdk.maps.Style;

public class MapActivity extends AppCompatActivity {
    Double lat, lng;
    private MapView mapView;
    String title;
    String addresse;

    public static final String MY_PREFS_NAME2 = "MAP";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Mapbox.getInstance(this, getString(R.string.mapbox_access_token));
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        lat =  this.getIntent().getDoubleExtra("lat", 2.365);
        lng =  this.getIntent().getDoubleExtra("long", 2.365);
        title = this.getIntent().getStringExtra("title");
        SharedPreferences prefs = this.getSharedPreferences(MY_PREFS_NAME2, MODE_PRIVATE);
        addresse =prefs.getString("addresse", "Tunis");


        mapView=findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);


        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {

/*                mapboxMap.setStyle(Style.MAPBOX_STREETS, new Style.OnStyleLoaded() {
                    @Override
                    public void onStyleLoaded(@NonNull Style style) {

// Map is set up and the style has loaded. Now you can add data or make other map adjustments.


                    }
                });*/
                // One way to add a marker view

                    mapboxMap.addMarker(new MarkerOptions()
                            .position(new LatLng(lat,lng))
                            .title(title)
                            .snippet(addresse)

                    );



                CameraPosition cameraPosition = null;

                    cameraPosition = new CameraPosition.Builder()
                            .target(new LatLng(lat, lng))
                            .zoom(15)
                            .build();

                mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 2000, null);

            }
        });





    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }


    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }


    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

}
