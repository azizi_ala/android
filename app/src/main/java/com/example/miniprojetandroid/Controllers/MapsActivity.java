package com.example.miniprojetandroid.Controllers;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;

import android.util.Log;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.example.miniprojetandroid.Map.LocationHandler;
import com.example.miniprojetandroid.Map.LocationResultListener;
import com.example.miniprojetandroid.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, LocationResultListener {

    private final int PERMISSION_REQUEST = 1000;
    private final int LOCATION_REQUEST_CODE = 2000;

    private GoogleMap googleMap;
    private LocationHandler locationHandler;


    private Button btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        locationHandler = new LocationHandler(this, this,
                LOCATION_REQUEST_CODE, PERMISSION_REQUEST);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


btn =findViewById(R.id.save);

    }






    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        locationHandler.getUserLocation();
        googleMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker"));

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;

        }
        googleMap.setMyLocationEnabled(true);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST){
            boolean isGranted = true;
            for (int i = 0; i < permissions.length; i++){
                if (grantResults[i] != PackageManager.PERMISSION_GRANTED){
                    isGranted = false;
                    break;
                }
            }
//            if (!isGranted){
//                new AlertDialog().Builder(this)
//                        .setTitle("Error")
//                        .setMessage("Cannot display location without enabling permission")
//                        .setPositiveButton("Ok", (dialog, which) -> locationHandler.getUserLocation())
//                        .setNegativeButton("Cancel",(dialog, which) -> dialog.dismiss())
//                        .create()
//                        .show();
//                        return;
//            }
            locationHandler.getUserLocation();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOCATION_REQUEST_CODE){
            if (resultCode == RESULT_OK){
                locationHandler.getUserLocation();
            }else{
                new AlertDialog.Builder(this)
                        .setTitle("Error")
                        .setMessage("Please enable location in order to display it on map")
                        .setPositiveButton("Enable", (dialog, which) -> locationHandler.getUserLocation())
                        .setNegativeButton("Cancel", ((dialog, which) -> dialog.dismiss()))
                        .create()
                        .show();
            }
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void getLocation(Location location) {
        googleMap.setMyLocationEnabled(true);
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions marker = new MarkerOptions().position(latLng);
        googleMap.addMarker(marker);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14));



        Log.d("ok" ,"laltitude "+latLng.toString());



//
//        btn.setOnClickListener(v -> {
//            Call<Checklist> call = new RetrofitConfig().getRetrofitService().getlocation(latLng.latitude+"",latLng.longitude+"");
//            call.enqueue(new Callback<Checklist>() {
//                @Override
//                public void onResponse(Call<Checklist> call, Response<Checklist> response) {
//                    Checklist i =response.body();
//                    if(response.isSuccessful()) {
//
//                        Log.e("hh ", "ok" +latLng.toString());
//                        Log.e("ok ", "msg" + response.code());
//
//
//                        SharedPrefManager.getInstance(MapActivity.this)
//                                .saveLocation(i);
//
//                        SharedPrefManager.getInstance(MapActivity.this).saveLocation(i);
//
//                        Intent intent = new Intent(MapActivity.this, AcceuilActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        startActivity(intent);
//
//                        Log.e("location " , " not ok " +i.toString() );
//
//                    }else{
//                        Log.e("hh " , " not ok " +latLng.toString() );
//                        Log.e("not ok " , "msg"  + response.code());
//
//
//
//                    }
//
//
//                }
//
//                @Override
//                public void onFailure(Call<Checklist> call, Throwable t) {
//                    Log.e("ERRUR_Service   ", "Error :" + t.getMessage());
//                }
//            });
//        });
//
//

    }












}