package com.example.miniprojetandroid.Controllers;


import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.miniprojetandroid.Entities.Evenement;
import com.example.miniprojetandroid.Entities.EvenementAdapter;
import com.example.miniprojetandroid.Entities.EvenementUserAdapter;
import com.example.miniprojetandroid.Entities.Participants;
import com.example.miniprojetandroid.Entities.User;
import com.example.miniprojetandroid.Entities.pubAdapter;
import com.example.miniprojetandroid.R;
import com.example.miniprojetandroid.Retrofit.INodeJS;
import com.example.miniprojetandroid.database.AppDataBase;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListEvenements extends Fragment {

    ImageView acceuil1,profileuser,favori;
    List<Evenement> evenementsList ;

    RecyclerView recyclerView;

    INodeJS myAPI;
    Context mContext;
    private AppDataBase database;
    private List<Evenement> event_list = new ArrayList<>();


    public ListEvenements() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        GetListEvenement();



        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_list_evenements, container, false);


        recyclerView = rootView.findViewById(R.id.publications);
        favori = rootView.findViewById(R.id.favori);

        return rootView;
    }



    public void GetListEvenement() {
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("http://192.168.8.100:1337")
                .addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = builder.build();
        myAPI = retrofit.create(INodeJS.class);
        Call<List<Evenement>> call = myAPI.getEventsList();
        call.enqueue(new Callback<List<Evenement>>() {
            @Override
            public void onResponse(Call<List<Evenement>> call, Response<List<Evenement>> response) {
                evenementsList = response.body();
                Log.d("test2", String.valueOf(response.body()));
                //recyclerView = (R.id.publications);
                EvenementAdapter adapter = new EvenementAdapter(mContext, evenementsList);
                recyclerView.setAdapter(adapter);
                recyclerView.setLayoutManager(new LinearLayoutManager(mContext));

            }

            @Override
            public void onFailure(Call<List<Evenement>> call, Throwable t) {

            }
        });
    }












}
