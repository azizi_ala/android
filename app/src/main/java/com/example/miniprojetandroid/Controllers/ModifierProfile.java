package com.example.miniprojetandroid.Controllers;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.miniprojetandroid.Entities.User;
import com.example.miniprojetandroid.R;
import com.example.miniprojetandroid.Retrofit.INodeJS;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class ModifierProfile extends Fragment implements View.OnClickListener {
    EditText modidier_nom, modifier_prenom;
    EditText c_mdp, n_mdp;
    SharedPreferences sharedPreferences;
    INodeJS myAPI;
    //Button enreg, save;


    public ModifierProfile() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_modifier_profile, container, false);











    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        modidier_nom = view.findViewById(R.id.mod_nom);
        modifier_prenom = view.findViewById(R.id.mod_prenom);
        //c_mdp = view.findViewById(R.id.motdepasseC);
        //n_mdp = view.findViewById(R.id.mod_motdepasse);
        view.findViewById(R.id.enregistrer).setOnClickListener(this);
        //view.findViewById(R.id.enreg_mdp).setOnClickListener(this);
    }
    private void updateProfile(int user_id){
        //sharedPreferences = getContext().getSharedPreferences("testt", Context.MODE_PRIVATE);
        //int userId = sharedPreferences.getInt("idUser",0);
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("http://192.168.8.100:1337")
                .addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = builder.build();
        myAPI = retrofit.create(INodeJS.class);
        Call<User> call = myAPI.updateProfile(user_id,modidier_nom.getText().toString(),modifier_prenom.getText().toString());
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {

                Toast.makeText(getContext(),response.message(),Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast.makeText(getContext(),t.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    public void onClick(View v) {
        sharedPreferences = getContext().getSharedPreferences("CurrentUser", Context.MODE_PRIVATE);
        int userId = sharedPreferences.getInt("idUser",0);
        System.out.println(String.valueOf(userId));
        switch (v.getId()){
            case R.id.enregistrer:
                updateProfile(userId);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("nomUser",modidier_nom.getText().toString());
                editor.putString("prenomUser",modifier_prenom.getText().toString());
                editor.apply();
                String a = modifier_prenom.getText().toString();
                System.out.println(a);
                break;
/*            case R.id.enreg_mdp:
                break;*/
        }

    }
}
