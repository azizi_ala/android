package com.example.miniprojetandroid.Controllers;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.miniprojetandroid.Entities.Evenement;
import com.example.miniprojetandroid.Map.Constants;
import com.example.miniprojetandroid.Map.MapAddPost;
import com.example.miniprojetandroid.Map.MapSearchDialog;
import com.example.miniprojetandroid.R;
import com.special.ResideMenu.ResideMenu;
import com.special.ResideMenu.ResideMenuItem;

import java.util.ArrayList;
import java.util.List;

public class MenuActivity extends AppCompatActivity implements MapSearchDialog.mapp{

    ImageButton menu ;
    ResideMenu resideMenu;
    ImageView acceuil1,profileuser,article,favori;
    List<Evenement> evenementsList ;
    String location="";
    boolean fetchAddress;
    int fetchType = Constants.USE_ADDRESS_LOCATION;
    AddressResultReceiver mResultReceiver;
    SharedPreferences sharedPreferencesU;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        evenementsList=new ArrayList<>();








// affichage pub event et action menu

        Fragment fragg = new ListEvenements();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.details, fragg);
        fragmentTransaction.commit();

        profileuser = findViewById(R.id.userProfile);
        acceuil1 = findViewById(R.id.acceuil1);
        article = findViewById(R.id.article);
        favori = findViewById(R.id.favori);

        acceuil1.setBackgroundColor(getResources().getColor(R.color.orange));
        article.setBackgroundColor(getResources().getColor(R.color.blue));
        favori.setBackgroundColor(getResources().getColor(R.color.blue));
        profileuser.setBackgroundColor(getResources().getColor(R.color.blue));

/*        if (favori.isInTouchMode()){
            favori.setBackgroundColor(getResources().getColor(R.color.orange));
        }else if(!favori.isInTouchMode()){favori.setBackgroundColor(getResources().getColor(R.color.menu));}*/


        favori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                favori.setBackgroundColor(getResources().getColor(R.color.orange));
                acceuil1.setBackgroundColor(getResources().getColor(R.color.blue));
                profileuser.setBackgroundColor(getResources().getColor(R.color.blue));
                article.setBackgroundColor(getResources().getColor(R.color.blue));
                Fragment fragg = new ModifierEvenement();
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.details, fragg);
                fragmentTransaction.commit();
            }
        });


        article.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                article.setBackgroundColor(getResources().getColor(R.color.orange));
                favori.setBackgroundColor(getResources().getColor(R.color.blue));
                profileuser.setBackgroundColor(getResources().getColor(R.color.blue));
                acceuil1.setBackgroundColor(getResources().getColor(R.color.blue));
                Fragment fragg = new ListArticles();
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.details, fragg);
                fragmentTransaction.commit();

            }
        });


        acceuil1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acceuil1.setBackgroundColor(getResources().getColor(R.color.orange));
                article.setBackgroundColor(getResources().getColor(R.color.blue));
                favori.setBackgroundColor(getResources().getColor(R.color.blue));
                profileuser.setBackgroundColor(getResources().getColor(R.color.blue));

                Fragment fragg = new ListEvenements();
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.details, fragg);
                fragmentTransaction.commit();
/*                Intent i = new Intent(MenuActivity.this, MenuActivity.class);
                startActivity(i);*/
            }
        });

        profileuser.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                profileuser.setBackgroundColor(getResources().getColor(R.color.orange));
                acceuil1.setBackgroundColor(getResources().getColor(R.color.blue));
                article.setBackgroundColor(getResources().getColor(R.color.blue));
                favori.setBackgroundColor(getResources().getColor(R.color.blue));

                Fragment fragg = new Profile();
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.details, fragg);
                fragmentTransaction.commit();

            }
        });






        menu = findViewById(R.id.menu);
        setMenu();



    }



    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {

        return resideMenu.dispatchTouchEvent(ev);
    }


    public void setMenu() {
        // attach to current activity;
        resideMenu = new ResideMenu(this);
        resideMenu.setBackground(R.drawable.backgroundmenu1);


        resideMenu.setShadowVisible(true);
        resideMenu.attachToActivity(this);

        // create menu items;

        String titles[] = {"ajout camping","Profil","map","Logout"};
        int icon[] = {R.drawable.add, R.drawable.add, R.drawable.profile, R.drawable.logout};

        for (int i = 0; i < titles.length; i++) {
            ResideMenuItem item = new ResideMenuItem(this, icon[i], titles[i]);
            if (titles[i].equals("Profil") ) {
                item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Fragment fragg = new ModifierProfile();
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.details, fragg);
                        fragmentTransaction.commit();
                        resideMenu.closeMenu();
                    }
                });
            } else if (titles[i].equals("ajout camping") ) {
                    item.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
/*                            MapSearchDialog dialog = new MapSearchDialog();
                            dialog.show(getSupportFragmentManager(), "map");*/
                            Fragment fragg = new AjouterEvenement();
                            FragmentManager fragmentManager = getSupportFragmentManager();
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.details, fragg);
                            fragmentTransaction.commit();
                            resideMenu.closeMenu();





                        }
                    });
                } else if (titles[i].equals("map") ) {
                    item.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
//                            MapSearchDialog dialog = new MapSearchDialog();
//                            dialog.show(getSupportFragmentManager(), "map");
                            Intent i = new Intent(MenuActivity.this, MapsActivity.class);
                            startActivity(i);

                        }
                    });




                } else if (titles[i].equals("Logout") ) {
                item.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sharedPreferencesU = getApplicationContext().getSharedPreferences("testt", Context.MODE_PRIVATE);
                        sharedPreferencesU.edit().clear().commit();
                        Intent i = new Intent(MenuActivity.this, LoginActivity.class);
                        startActivity(i);


                    }
                });

            }
            resideMenu.addMenuItem(item, ResideMenu.DIRECTION_LEFT); // or  ResideMenu.DIRECTION_RIGHT
        }




        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resideMenu.openMenu(ResideMenu.DIRECTION_LEFT);
            }
        });






        ResideMenu.OnMenuListener menuListener = new ResideMenu.OnMenuListener() {
            @Override
            public void openMenu() {
                Toast.makeText(getApplicationContext(), "Menu is opened!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void closeMenu() {
                Toast.makeText(getApplicationContext(), "Menu is closed!", Toast.LENGTH_SHORT).show();
            }
        };
        resideMenu.setMenuListener(menuListener);


    }


    @Override
    public void getlocation(String location) {
        this.location = location;
        fetchAddress = false;
        fetchType = Constants.USE_ADDRESS_NAME;


        Intent intent = new Intent(getApplicationContext(), com.example.miniprojetandroid.Map.GeocodeAddressIntentService.class);
        intent.putExtra(Constants.RECEIVER, mResultReceiver);
        intent.putExtra(Constants.FETCH_TYPE_EXTRA, fetchType);
        if (fetchType == Constants.USE_ADDRESS_NAME) {

            intent.putExtra(Constants.LOCATION_NAME_DATA_EXTRA, location);
//                                    Log.d("cafeine", location);
        }
        startService(intent);

    }

    class AddressResultReceiver extends ResultReceiver {
        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, final Bundle resultData) {
            if (resultCode == Constants.SUCCESS_RESULT) {
                final Address address = resultData.getParcelable(Constants.RESULT_ADDRESS);
                MenuActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {


                        Intent i = new Intent(MenuActivity.this, MapAddPost.class);
                        i.putExtra("lat", address.getLatitude());
                        i.putExtra("lng", address.getLongitude());
                        startActivity(i);
                        MenuActivity.this.finish();

                    }
                });
            } else {
                MenuActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
/*                         progressBar.setVisibility(View.GONE);
                        infoText.setVisibility(View.VISIBLE);
                        infoText.setText(resultData.getString(Constants.RESULT_DATA_KEY));*/

                        Toast.makeText(getApplicationContext(), resultData.getString(Constants.RESULT_DATA_KEY), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    }
}
