package com.example.miniprojetandroid.dao;


import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;


import com.example.miniprojetandroid.Entities.Evenement;
import com.example.miniprojetandroid.Entities.Participants;

import java.util.List;

@androidx.room.Dao
public interface Dao {

    @Insert
    void insertOne(Evenement evenement);
    @Query("SELECT * FROM evenement_table WHERE id_evenement=:id")
    int check_item(int id);
    @Delete
    void delete(Evenement produit);
    @Query("SELECT * FROM evenement_table")
    List<Evenement> getAll();


}
