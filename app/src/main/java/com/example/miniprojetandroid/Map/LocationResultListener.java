package com.example.miniprojetandroid.Map;

import android.location.Location;

public interface LocationResultListener {
    void getLocation(Location location);
}