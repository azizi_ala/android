package com.example.miniprojetandroid.Map;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.miniprojetandroid.R;

public class MapSearchDialog extends AppCompatDialogFragment {

    EditText editText;
    Button button;
    private mapp listener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.map_dialog, null);
        button = view.findViewById(R.id.find);
        editText = view.findViewById(R.id.mapplace);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.getlocation(editText.getText().toString());
                System.out.println(editText.getText().toString());
                dismiss();

            }

        });
        builder.setView(view)
                .setTitle("Choisir un emplacement");
        return builder.create();


    }
    @Override
    public void onAttach(Context context) {
        try {
            listener = (mapp) getActivity();
        } catch (ClassCastException e) {
            System.out.println("must implement interface");
        }
        super.onAttach(context);
    }

    public interface mapp {
        void getlocation(String location);
    }
}
